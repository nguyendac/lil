<div class="footer_top">
  <ul>
    <li><a href="./contact.html">お問い合わせ</a></li>
    <li><a href="./privacy.html">プライバシーポリシー</a></li>
    <li><a href="./about.html">このサイトについて</a></li>
    <li><a href="./company.html">運営会社</a></li>
  </ul>
</div>
<div class="footer_bottom">Copyright ©XPRESS CO.,LTD. All Right Reserved.</div>