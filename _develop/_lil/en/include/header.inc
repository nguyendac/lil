<div class="header_wrap inner">
  <div class="hamburger hamburger--spin show_sp" id="hamburger">
    <div class="hamburger-box">
      <div class="hamburger-inner"></div>
    </div>
  </div>
  <h1><a href="./"><img src="/asset/img/logo.svg" alt="logo"></a></h1>
  <nav class="header_nav" id="nav">
    <ul class="header_list">
      <!-- <li><a href="/#news" class="anchor">WHAT’S NEW</a></li> -->
      <li><a href="./#what_lil" class="anchor">ABOUT</a></li>
      <li><a href="./#kodawari" class="anchor">LIL’ APPROACH</a></li>
      <li><a href="./#menu" class="anchor">MENU</a></li>
      <li><a href="./#shop" class="anchor">SHOP INFORMATION</a></li>
      <!-- <li><a href="#">EMPLOYMENT</a></li> -->
    </ul>
    <ul class="header_social">
      <!-- <li><a href="#"><img src="./asset/img/icon_face.svg" alt="facebook"></a></li> -->
      <li><a href="https://twitter.com/LilDonuts" class="tw" target="blank">Twitter</a></li>
      <li><a href="https://www.instagram.com/lildonuts_tw/" class="ins" target="blank">Instagram</a></li>
      <!-- <li><a href="#"><img src="./asset/img/icon_line.svg" alt="line"></a></li> -->
    </ul>
  </nav>
  <div id="lang" class="header_lang en" data-en="ENG" data-jp="日本語"><span></span></div>
</div>