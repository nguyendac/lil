<section class="st_happiness" id="happiness">
  <div class="row wrap">
    <h2 class="ttl_hp">「できたての幸せ」を<br>お届けしたい</h2>
    <p>
      温かいドーナツやクレープを頬張ると、<br>
      やさしさに包まれた気がして不意に笑みがこぼれます。<br>
      特別感や華やかさはないけれど、<br>
      ほんのりした甘さのドーナツやクレープを頬張るたびに気付く小さな幸せ。<br>
      私たちは、特別な日も、普段の日にも、そして手土産にも、<br>
      甘い香りととも小さな幸せを届けます。
    </p>
    <span><img src="./asset/img/au_hp_pc.svg" alt="Lil Happiness"></span>
  </div>
  <!--/.wrap-->
</section>
<!--/.st_happiness-->
<section class="st_movie" id="movie">
  <div class="row inner">
    <h2><img src="./asset/img/ttl_movie.svg" alt="movie"></h2>
    <a id="click_modal" class="ev_movie" href="#"><img src="./asset/img/video_pc.png" class="resimg" alt="video"></a>
  </div>
  <!--/.wrap-->
</section>
<!--/.st_movie-->
<section class="st_kodawari" id="kodawari">
  <div class="row inner">
    <h2><img src="./asset/img/ttl_kodawari.svg" alt="kodawari"></h2>
    <div class="gr_kodawari">
      <div class="gr_kodawari_item">
        <article>
          <span><img src="./asset/img/n_01_pc.svg" class="resimg" alt="01"></span>
          <h3>『生地』</h3>
          <p>
            ドーナツ粉に使用している小麦は全て「北海道産小麦」。<br>
            良質な小麦を使ったオリジナル配合のドーナツ粉に、有機大豆を使った有機豆乳をたっぷりと加えて練り上げています。<br>
            揚げたてはもちろん、冷めてもしっとりおいしくお召し上がりいただけます。
          </p>
        </article>
        <article>
          <span><img src="./asset/img/n_02_pc.svg" class="resimg" alt="02"></span>
          <h3>『揚げたて』</h3>
          <p>
            リルドーナツではオーガニックオイルを使用し、いつでもご注文ごとにドーナツを揚げます。<br>
            ご注文をいただいてから商品を作りますので少々お待ちいただきますが、ドーナツのおいしさを味わってもらうための、リルドーナツ1番のこだわりです。
          </p>
        </article>
        <article>
          <span><img src="./asset/img/n_03_pc.svg" class="resimg" alt="03"></span>
          <h3>『Lil’スタイル』</h3>
          <p>
            リルドーナツは他の店とはちょっと違います。持ち手付きのボックス型パッケージに入ったミニドーナツを木製のフォークで食べるのが特徴です。<br>
            お子様でも食べやすく、手を汚さずに食べることができます。
          </p>
        </article>
      </div>
      <!--/.item-->
      <div class="btn"><a href="#"><span>MORE</span></a></div>
    </div>
    <!--/.gr_kodawari-->
  </div>
  <!--/.wrap-->
</section>
<!--/.st_kodawari-->
