window.requestAnimFrame = (function(callback) {
  return window.requestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.oRequestAnimationFrame ||
    window.msRequestAnimationFrame ||
    function(callback) {
      return window.setTimeout(callback, 1000 / 60);
    };
})();

window.cancelAnimFrame = (function(_id) {
  return window.cancelAnimationFrame ||
    window.cancelRequestAnimationFrame ||
    window.webkitCancelAnimationFrame ||
    window.webkitCancelRequestAnimationFrame ||
    window.mozCancelAnimationFrame ||
    window.mozCancelRequestAnimationFrame ||
    window.msCancelAnimationFrame ||
    window.msCancelRequestAnimationFrame ||
    window.oCancelAnimationFrame ||
    window.oCancelRequestAnimationFrame ||
    function(_id) { window.clearTimeout(id); };
})();

function closest(el, selector) {
  // type el -> Object
  // type select -> String
  var matchesFn;
  // find vendor prefix
  ['matches', 'webkitMatchesSelector', 'mozMatchesSelector', 'msMatchesSelector', 'oMatchesSelector'].some(function(fn) {
    if (typeof document.body[fn] == 'function') {
      matchesFn = fn;
      return true;
    }
    return false;
  })
  var parent;
  // traverse parents
  while (el) {
    parent = el.parentElement;
    if (parent && parent[matchesFn](selector)) {
      return parent;
    }
    el = parent;
  }
  return null;
}

function getCssProperty(elem, property) {
  return window.getComputedStyle(elem, null).getPropertyValue(property);
}
var easingEquations = {
  easeOutSine: function(pos) {
    return Math.sin(pos * (Math.PI / 2));
  },
  easeInOutSine: function(pos) {
    return (-0.5 * (Math.cos(Math.PI * pos) - 1));
  },
  easeInOutQuint: function(pos) {
    if ((pos /= 0.5) < 1) {
      return 0.5 * Math.pow(pos, 5);
    }
    return 0.5 * (Math.pow((pos - 2), 5) + 2);
  }
};

function isPartiallyVisible(el) {
  var elementBoundary = el.getBoundingClientRect();
  var top = elementBoundary.top;
  var bottom = elementBoundary.bottom;
  var height = elementBoundary.height;
  return ((top + height >= 0) && (height + window.innerHeight >= bottom));
}

function isFullyVisible(el) {
  var elementBoundary = el.getBoundingClientRect();
  var top = elementBoundary.top;
  var bottom = elementBoundary.bottom;
  return ((top >= 0) && (bottom <= window.innerHeight));
}

function CreateElementWithClass(elementName, className) {
  var el = document.createElement(elementName);
  el.className = className;
  return el;
}

function createElementWithId(elementName, idName) {
  var el = document.createElement(elementName);
  el.id = idName;
  return el;
}

function getScrollbarWidth() {
  var outer = document.createElement("div");
  outer.style.visibility = "hidden";
  outer.style.width = "100px";
  document.body.appendChild(outer);
  var widthNoScroll = outer.offsetWidth;
  // force scrollbars
  outer.style.overflow = "scroll";
  // add innerdiv
  var inner = document.createElement("div");
  inner.style.width = "100%";
  outer.appendChild(inner);
  var widthWithScroll = inner.offsetWidth;
  // remove divs
  outer.parentNode.removeChild(outer);
  return widthNoScroll - widthWithScroll;
}
var transform = ["transform", "msTransform", "webkitTransform", "mozTransform", "oTransform"];
var flex = ['-webkit-box', '-moz-box', '-ms-flexbox', '-webkit-flex', 'flex'];
var fd = ['flexDirection', '-webkit-flexDirection', '-moz-flexDirection'];
var animatriondelay = ["animationDelay", "-moz-animationDelay", "-wekit-animationDelay"];

function getSupportedPropertyName(properties) {
  for (var i = 0; i < properties.length; i++) {
    if (typeof document.body.style[properties[i]] != "undefined") {
      return properties[i];
    }
  }
  return null;
}
var transformProperty = getSupportedPropertyName(transform);
var flexProperty = getSupportedPropertyName(flex);
var fdProperty = getSupportedPropertyName(fd);
var ad = getSupportedPropertyName(animatriondelay);

function detectIE(){
  var ua = window.navigator.userAgent;
  var msie = ua.indexOf('MSIE ');
  var trident = ua.indexOf('Trident/');
  if (msie > 0 || trident > 0) {
    // IE 10 or older => return version number
    // return 'ie'+parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    return 'ie';
  }
  return false;
}
function detect7() {
  var ua = window.navigator.userAgent;
  var isWin7 = ua.indexOf('Windows NT 6.1');
  if (isWin7 > 0) {
    return 'win7';
  }
  return false;
};

window.addEventListener('DOMContentLoaded', function() {
  if(detectIE()){
    document.body.classList.add(detectIE());
  }
  if(detect7()) {
    document.body.classList.add(detect7());
  }
  new Menu();
  new Sticky();
  new Anchor();
  new Lang();
});
/* images pc <---> sp */
(function() {
  var PicturePolyfill = (function() {
    function PicturePolyfill() {
      var _this = this;
      this.pictures = [];
      this.onResize = function() {
        var width = document.body.clientWidth;
        for (var i = 0; i < _this.pictures.length; i = (i + 1)) {
          _this.pictures[i].update(width);
        };
      };
      if ([].includes) return;
      var picture = Array.prototype.slice.call(document.getElementsByTagName('picture'));
      for (var i = 0; i < picture.length; i = (i + 1)) {
        this.pictures.push(new Picture(picture[i]));
      };
      window.addEventListener("resize", this.onResize, false);
      this.onResize();
    }
    return PicturePolyfill;
  }());
  var Picture = (function() {
    function Picture(node) {
      var _this = this;
      this.update = function(width) {
        width <= _this.breakPoint ? _this.toSP() : _this.toPC();
      };
      this.toSP = function() {
        if (_this.isSP) return;
        _this.isSP = true;
        _this.changeSrc();
      };
      this.toPC = function() {
        if (!_this.isSP) return;
        _this.isSP = false;
        _this.changeSrc();
      };
      this.changeSrc = function() {
        var toSrc = _this.isSP ? _this.srcSP : _this.srcPC;
        _this.img.setAttribute('src', toSrc);
      };
      this.img = node.getElementsByTagName('img')[0];
      this.srcPC = this.img.getAttribute('src');
      var source = node.getElementsByTagName('source')[0];
      this.srcSP = source.getAttribute('srcset');
      this.breakPoint = Number(source.getAttribute('media').match(/(\d+)px/)[1]);
      this.isSP = !document.body.clientWidth <= this.breakPoint;
      this.update();
    }
    return Picture;
  }());
  new PicturePolyfill();
}());
var Lang =  (function(){
  function Lang(){
    var l = this;
    var getUrl = window.location.origin;
    this._target = document.getElementById('lang');
    this._circle = this._target.querySelector('span');
    this._base = window.location.origin;
    this._path =  window.location.pathname.split('/');
    this._link = this._path.pop();
    this._circle.addEventListener('click',function(e){
      e.preventDefault();
      if(l._target.classList.contains('en')) {
        var link  = l._base+'/'+l._link;
        window.location.href = link;
      } else {
        var link  = l._base+'/en/'+l._link;
        if(l._link == 'recruit.html') {
          link  = l._base+'/en/';
        }
        window.location.href = link;
      }
    })
  }
  return Lang;
})()
var Menu = (function() {
  function Menu() {
    var m = this;
    this._target = document.getElementById('hamburger');
    this._mobile = document.getElementById('nav');
    this._header = document.getElementById('header');
    this.flag_start = true;
    this._getbuffer = function() {
      var _buffer;
      _buffer = m._header.clientHeight;
      return _buffer;
    }
    this._buffer = this._getbuffer();
    this.scrollToY = function(scrollTargetY,speed,easing){
      var scrollY = window.scrollY || window.pageYOffset,
        scrollTargetY = scrollTargetY || 0,
        speed = speed || 2000,
        easing = easing || 'easeOutSine',
        currentTime = 0;
      var time = Math.max(.1, Math.min(Math.abs(scrollY - scrollTargetY) / speed, .8));
      function tick() {
        if(m.flag_start){
          currentTime += 1 / 60;
          var p = currentTime / time;
          var t = easingEquations[easing](p);
          if (p < 1) {
            requestAnimFrame(tick);
            window.scrollTo(0, scrollY + ((scrollTargetY - scrollY) * t));
          } else {
            window.scrollTo(0, scrollTargetY);
          }
        }
      }
      tick();
    }
    this._target.addEventListener('click', function() {
      if (this.classList.contains('open')) {
        this.classList.remove('open');
        m._mobile.classList.remove('open');
        document.body.style.overflow = 'inherit';
      } else {
        var _top = document.documentElement.scrollTop || document.body.scrollTop;
        if(document.getElementById('news')){
          if(_top < document.getElementById('news').offsetTop) {
            m.scrollToY((document.getElementById('news').offsetTop - m._buffer),1500,'easeOutSine');
            // this.classList.add('open');
            // m._mobile.classList.add('open');
            // document.body.style.overflow = 'hidden';
          }
        }
        this.classList.add('open');
        m._mobile.classList.add('open');
        document.body.style.overflow = 'hidden';
      }
    })
    this._reset = function() {
      if (m._target.classList.contains('open')) {
        if (window.innerWidth > 767) {
          m._target.classList.remove('open');
          m._mobile.classList.remove('open');
          document.body.style.overflow = 'auto';
          m._mobile.style.height = 'auto';
          document.body.style.paddingTop = m._header.clientHeight + 'px';
          m._mobile.style.height = 'auto';
        } else {
          m._mobile.style.height = window.innerHeight - closest(m._target, 'header').clientHeight + 'px';
          m._mobile.style.top = m._header.clientHeight + 'px';
        }
      }
    }

    this._reset();
    window.addEventListener('resize', m._reset, false);
  }
  return Menu;
})()
var Sticky = (function() {
  function Sticky() {
    var s = this;
    this._target = document.getElementById('header');
    // this._fv = document.getElementById('fv');
    this.flg = (document.getElementById('fv'))?document.getElementById('fv').clientHeight: 0;
    this._for_sp = function(top) {
      // document.body.style.paddingTop = s._target.clientHeight + 'px';
      if (top >= s.flg) {
        s._target.classList.add('fixed');
        document.getElementById('wrapper').style.paddingTop = s._target.clientHeight + 'px';
      } else {
        s._target.classList.remove('fixed');
        document.getElementById('wrapper').style.paddingTop = 0;
      }
    }
    this._for_pc = function(top, left) {
      if (top >= s.flg) {
        s._target.classList.add('fixed');
        document.getElementById('wrapper').style.paddingTop = s._target.clientHeight + 'px';
      } else {
        s._target.classList.remove('fixed');
        document.getElementById('wrapper').style.paddingTop = 0;
      }
    }
    this.handling = function() {
      var _top = document.documentElement.scrollTop || document.body.scrollTop;
      var _left = document.documentElement.scrollLeft || document.body.scrollLeft;
      s.flg = (document.getElementById('fv'))?document.getElementById('fv').clientHeight: 0;
      if (window.innerWidth < 768) {
        s._for_sp(_top);
      } else {
        if (!s._target.classList.contains('top')) {
          s._target.classList.remove('fixed')
        }
        s._for_pc(_top, _left);
      }
    }
    window.addEventListener('scroll', s.handling, false);
    window.addEventListener('resize', s.handling, false);
    window.addEventListener('load', s.handling, false);
  }
  return Sticky;
})()
var Anchor = (function(){
  function Anchor(){
    var a = this;
    this._target = '.anchor';
    this._header = document.getElementById('header');
    this.timer;
    this.flag_start = false;
    this.iteration;
    this.eles = document.querySelectorAll(this._target);
    this.stopEverything = function(){
      a.flag_start = false;
    }
    this._getbuffer = function() {
      var _buffer;
      _buffer = a._header.clientHeight;
      return _buffer;
    }
    this._buffer = this._getbuffer();
    this.scrollToY = function(scrollTargetY,speed,easing){
      var scrollY = window.scrollY || window.pageYOffset,
        scrollTargetY = scrollTargetY || 0,
        speed = speed || 2000,
        easing = easing || 'easeOutSine',
        currentTime = 0;
      var time = Math.max(.1, Math.min(Math.abs(scrollY - scrollTargetY) / speed, .8));
      function tick() {
        if(a.flag_start){
          currentTime += 1 / 60;
          var p = currentTime / time;
          var t = easingEquations[easing](p);
          if (p < 1) {
            requestAnimFrame(tick);
            window.scrollTo(0, scrollY + ((scrollTargetY - scrollY) * t));
          } else {
            window.scrollTo(0, scrollTargetY);
          }
        }
      }
      tick();
    }
    Array.prototype.forEach.call(this.eles,function(el,i){
      el.addEventListener('click',function(e){
        var next = el.getAttribute('href').split('#')[1];
        if(document.getElementById(next)){
          a.flag_start = true;
          e.preventDefault();
          a.scrollToY((document.getElementById(next).offsetTop-a._buffer),1500,'easeOutSine');
          if(window.innerWidth < 768 && !this.classList.contains('arrow_down')) {
            document.getElementById('hamburger').click();
          }
        }
      })
    });
    this._start = function(){
      var next = window.location.hash.split('#')[1];
      a.flag_start = true;
      if(next){
        a.scrollToY((document.getElementById(next).offsetTop - a._buffer),1500,'easeOutSine');
      }
    }
    window.addEventListener('load',a._start,false);
    document.querySelector("body").addEventListener('mousewheel',a.stopEverything,false);
    document.querySelector("body").addEventListener('DOMMouseScroll',a.stopEverything,false);
  }
  return Anchor;
})();
// JavaScript Document

var ResponsiveWatcher = (function () {
  function ResponsiveWatcher() {
    var _this = this;
    this.responsiveRatio = 768;
    this.isSP = window.innerWidth >= this.responsiveRatio ? true : false;
    this.isFirstTrigger = true;
    this.domRespnosiveImage = document.getElementsByClassName('resimg');
    this.init = function () {
      window.addEventListener("resize", _this.OnResize, false);
    };
    this.OnResize = function () {
      var i = 0 | 0;
      if (window.innerWidth >= _this.responsiveRatio && _this.isSP) {
        _this.isSP = false;
        for (i = 0; i < _this.domRespnosiveImage.length; i = (i + 1) | 0) {
          _this.domRespnosiveImage[i].setAttribute("src", _this.domRespnosiveImage[i].getAttribute("src").replace("_sp.", "_pc."));
        };
      }
      if (window.innerWidth < _this.responsiveRatio && !_this.isSP) {
        _this.isSP = true;
        for (i = 0; i < _this.domRespnosiveImage.length; i = (i + 1) | 0) {
          _this.domRespnosiveImage[i].setAttribute("src", _this.domRespnosiveImage[i].getAttribute("src").replace("_pc.", "_sp."));
        };
      }
      if (_this.isFirstTrigger) {
        _this.isFirstTrigger = false;
        for (i = 0; i < _this.domRespnosiveImage.length; i = (i + 1) | 0) {
          _this.domRespnosiveImage[i].style.visibility = "visible";
        }
      }
    };
    window.addEventListener('DOMContentLoaded', this.init);
    this.OnResize();
  }
  return ResponsiveWatcher;
}());

new ResponsiveWatcher();