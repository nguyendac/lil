window.addEventListener('DOMContentLoaded',function(){
  new According();
})
var According = (function(){
function According(){
  var a = this;
  this._targets = $('.according_event');
  this._anchor = window.location.hash.split('#')[1];
  this._width = window.innerWidth;
  this._targets.each(function(){
    $(this).click(function(e){
      e.preventDefault();
      if(window.innerWidth > 767) {
        return;
      }
      if($(this).closest('.according').hasClass('active')){
        $(this).closest('.according').removeClass('active');
        $(this).next().slideUp();
      } else {
        $(this).closest('.according').addClass('active');
        $(this).next().slideDown();
      }
      $(this).closest('.according').siblings().removeClass('active');
      $(this).closest('.according').siblings().find('.according_obj').slideUp();
    })
  })
  window.addEventListener('resize',function(){
    if(window.innerWidth > 767) {
      a._targets.each(function(){
        $(this).next().slideDown();
      }) 
    } else {
      a._targets.each(function(){
        if(a._width != window.innerWidth) {
          a._width = window.innerWidth;
          if(a._anchor) {
            if($(this).closest('.according').is('#'+a._anchor)) {
              $(this).next().slideDown();
            } else {
              $(this).next().slideUp();
            }
          }
        }
      })
      
    }
  })
  window.addEventListener('load',function(){
    if(window.innerWidth > 767){
      a._targets.each(function(){
        $(this).next().slideDown();
      })
    } else {
      a._targets.each(function(){
        $(this).next().slideUp();
        if(a._anchor) {
          if($(this).closest('.according').is('#'+a._anchor)) {
            $(this).next().slideDown();
          }
        }
      })
    }
  })
}
return According;
})()