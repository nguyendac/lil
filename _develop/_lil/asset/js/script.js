// create youtube player
  var player;
  function onYouTubePlayerAPIReady() {
      player = new YT.Player('player', {
        height: '390',
        width: '640',
        videoId: 'EpHXvZutfmw',
        events: {
          // 'onReady': onPlayerReady,
          'onStateChange': onPlayerStateChange
        }
      });
  }

  // stopplay video
  function onPlayerReady(event) {
      event.target.stopVideo();
  }
  // when video ends
  function onPlayerStateChange(event) {
    if(event.data === 0) {
      player.stopVideo();
      document.getElementById('close').click();
    }
  }
window.addEventListener('DOMContentLoaded', function() {
  $('.slick').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    dots: false,
    responsive: [
      {
        breakpoint: 769,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      }
    ]
  })
  new Effect();
  new Popup();
  Array.prototype.forEach.call(document.querySelectorAll('.parallax'),function(el){
    new Parallax(el);
  })
  $(document).on('click','.news_slide li > a',function(e){
    e.preventDefault();
    var t = $(this);
    t.parent().addClass('active');
    t.parent().siblings().removeClass('active');
  })
  $(document).on('click','.extra .close',function(e){
    e.preventDefault();
    var t = $(this);
    t.closest('li').removeClass('active');
  })
});
var Popup = (function(){
  function Popup(){
    var p = this;
    this._target = document.getElementById('popup');
    this._content =  document.getElementById('content_popup');
    this._obj = document.getElementById('click_modal');
    this._obj.addEventListener('click',function(e){
      e.preventDefault();
      player.playVideo();
      p._target.classList.add('open');
      document.body.style.overflow = 'hidden';
    });
    this._target.querySelector('.close').addEventListener('click',function(e){
      e.preventDefault();
      player.pauseVideo();
      p._target.classList.remove('open');
      document.body.style.overflow = 'inherit';
      // onPlayerStateChange();
    })
    this._target.addEventListener('click',function(e){
      if(e.target.id == 'popup') {
        p._target.querySelector('.close').click();
      }
    })
  }
  return Popup;
})();
var Effect = (function(){
  function Effect(){
    var e = this;
    this.eles = document.querySelectorAll('.effect');
    this.handling = function(){
      var _top  = document.documentElement.scrollTop
      Array.prototype.forEach.call(e.eles,function(el,i){
        if(isPartiallyVisible(el)){
          el.classList.add('active');
        }
        if(el.classList.contains('parallax')) {
          if(isPartiallyVisible(el)){
            el.classList.add('active');
          } else {
            el.classList.remove('active');
          }
        }
      })
    }
    window.addEventListener('scroll',e.handling,false);
    this.handling();
  }
  return Effect;
})();
var Parallax = (function(){
  function Parallax(el) {
    var p = this;
    this._curr = 0;
    this._step = 2;
    var lastScrollTop = 0;
    window.addEventListener("scroll", function(){
     var st = window.pageYOffset || document.documentElement.scrollTop;
     if(el.classList.contains('active')) {
       if (st > lastScrollTop){
        p._curr -= p._step;
       } else {
         p._curr += p._step;;
       }
     el.querySelector('#airship').style[transformProperty] = "translateY(" + (p._curr) + "px)";
     } else {
       el.querySelector('#airship').style[transformProperty] = "translateY(0px)";
       p._curr = 0;
     }
    lastScrollTop = st <= 0 ? 0 : st;
    }, false);
  }
  return Parallax;
})();
/* images pc <---> sp */
(function() {
  var PicturePolyfill = (function() {
    function PicturePolyfill() {
      var _this = this;
      this.pictures = [];
      this.onResize = function() {
        var width = document.body.clientWidth;
        for (var i = 0; i < _this.pictures.length; i = (i + 1)) {
          _this.pictures[i].update(width);
        };
      };
      if ([].includes) return;
      var picture = Array.prototype.slice.call(document.getElementsByTagName('picture'));
      for (var i = 0; i < picture.length; i = (i + 1)) {
        this.pictures.push(new Picture(picture[i]));
      };
      window.addEventListener("resize", this.onResize, false);
      this.onResize();
    }
    return PicturePolyfill;
  }());
  var Picture = (function() {
    function Picture(node) {
      var _this = this;
      this.update = function(width) {
        width <= _this.breakPoint ? _this.toSP() : _this.toPC();
      };
      this.toSP = function() {
        if (_this.isSP) return;
        _this.isSP = true;
        _this.changeSrc();
      };
      this.toPC = function() {
        if (!_this.isSP) return;
        _this.isSP = false;
        _this.changeSrc();
      };
      this.changeSrc = function() {
        var toSrc = _this.isSP ? _this.srcSP : _this.srcPC;
        _this.img.setAttribute('src', toSrc);
      };
      this.img = node.getElementsByTagName('img')[0];
      this.srcPC = this.img.getAttribute('src');
      var source = node.getElementsByTagName('source')[0];
      this.srcSP = source.getAttribute('srcset');
      this.breakPoint = Number(source.getAttribute('media').match(/(\d+)px/)[1]);
      this.isSP = !document.body.clientWidth <= this.breakPoint;
      this.update();
    }
    return Picture;
  }());
  new PicturePolyfill();
}());
(function() {
  if (document.querySelector('.news_list_border')) {
    var scrollContainer = document.querySelector('.scrollable'),
      scrollContentWrapper = document.querySelector('.bx_rules'),
      scrollContent = document.querySelector('.bx_rules .bx_content'),
      contentPosition = 0,
      scrollerBeingDragged = false,
      scroller,
      topPosition,
      scrollerHeight;

    function calculateScrollerHeight() {
      // *Calculation of how tall scroller should be
      var visibleRatio = scrollContainer.offsetHeight / scrollContentWrapper.scrollHeight;
      return visibleRatio * scrollContainer.offsetHeight*3;
    }

    function moveScroller(evt) {
      // Move Scroll bar to top offset
      var scrollPercentage = evt.target.scrollTop / scrollContentWrapper.scrollHeight;
      topPosition = scrollPercentage * (scrollContainer.offsetHeight - 5);
      scroller.style.top = topPosition + 'px';
    }

    function startDrag(evt) {
      normalizedPosition = evt.pageY;
      contentPosition = scrollContentWrapper.scrollTop;
      scrollerBeingDragged = true;
    }

    function stopDrag(evt) {
      scrollerBeingDragged = false;
    }

    function scrollBarScroll(evt) {
      if (scrollerBeingDragged === true) {
        var mouseDifferential = evt.pageY - normalizedPosition;
        var scrollEquivalent = mouseDifferential * (scrollContentWrapper.scrollHeight / scrollContainer.offsetHeight);
        scrollContentWrapper.scrollTop = contentPosition + scrollEquivalent;
      }
    }

    function createScroller() {
      // *Creates scroller element and appends to '.scrollable' div
      // create scroller element
      scroller = document.createElement("div");
      scroller.className = 'scroller';
      // determine how big scroller should be based on content
      scrollerHeight = calculateScrollerHeight();
      if (scrollerHeight / scrollContainer.offsetHeight < 1) {
        // *If there is a need to have scroll bar based on content size
        scroller.style.height = scrollerHeight + 'px';
        // append scroller to scrollContainer div
        scrollContainer.appendChild(scroller);
        // show scroll path divot
        scrollContainer.className += ' showScroll';
        // attach related draggable listeners
        scroller.addEventListener('mousedown', startDrag);
        window.addEventListener('mouseup', stopDrag);
        window.addEventListener('mousemove', scrollBarScroll)
      }
    }
    createScroller();
    // *** Listeners ***
    scrollContentWrapper.addEventListener('scroll', moveScroller);
  }
}());


