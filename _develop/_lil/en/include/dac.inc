<div id="news" class="news parallax effect">
  <div class="news_row row">
    <div class="news_inner inner">
      <div class="news_slide">
        <ul class="slick">
          <li><a href="#">
              <figure>
                <img src="./asset/img/lil_product@2x.png" alt="lil product">
                <figcaption>新商品名が入ります</figcaption>
              </figure>
            </a></li>
          <li><a href="#">
              <figure>
                <img src="./asset/img/lil_product@2x.png" alt="lil product">
                <figcaption>新商品名が入ります<br>2行目</figcaption>
              </figure>
            </a></li>
          <li><a href="#">
              <figure>
                <img src="./asset/img/lil_product@2x.png" alt="lil product">
                <figcaption>新商品名が入ります<br>2行目</figcaption>
              </figure>
            </a></li>
            <li><a href="#">
                <figure>
                  <img src="./asset/img/lil_product@2x.png" alt="lil product">
                  <figcaption>新商品名が入ります</figcaption>
                </figure>
              </a></li>
            <li><a href="#">
                <figure>
                  <img src="./asset/img/lil_product@2x.png" alt="lil product">
                  <figcaption>新商品名が入ります<br>2行目</figcaption>
                </figure>
              </a></li>
            <li><a href="#">
                <figure>
                  <img src="./asset/img/lil_product@2x.png" alt="lil product">
                  <figcaption>新商品名が入ります<br>2行目</figcaption>
                </figure>
              </a></li>
        </ul>
      </div>
      <div class="news_btn">
        <a href="#">商品一覧へ</a>
      </div>
      <div class="news_list">
        <div class="news_list_border">
          <div class="news_list_w">
            <div class="scrollable">
              <div class="bx_rules">
                <div class="bx_content">
                  <ul>
                    <li><a href="#">
                        <time datetime="2018-01-01">2018.00.00</time>
                        <span class="cat_01">豊洲店</span>
                        <em>最新のニュースを掲載します。CMS対応にてお願いいたします。（最大50文字程度）</em>
                      </a></li>
                    <li><a href="#">
                        <time datetime="2018-01-01">2018.00.00</time>
                        <span class="cat_02">入間店</span>
                        <em>最新のニュースを掲載します。CMS対応にてお願いいたします。（最大50文字程度）</em>
                      </a></li>
                    <li><a href="#">
                        <time datetime="2018-01-01">2018.00.00</time>
                        <span class="cat_03">札幌北広島店</span>
                        <em>最新のニュースを掲載します。CMS対応にてお願いいたします。（最大50文字程度）</em>
                      </a></li>
                    <li><a href="#">
                        <time datetime="2018-01-01">2018.00.00</time>
                        <span class="cat_04">台湾店</span>
                        <em>最新のニュースを掲載します。CMS対応にてお願いいたします。（最大50文字程度）</em>
                      </a></li>
                    <li><a href="#">
                        <time datetime="2018-01-01">2018.00.00</time>
                        <span class="cat_01">豊洲店</span>
                        <em>最新のニュースを掲載します。CMS対応にてお願いいたします。（最大50文字程度）</em>
                      </a></li>
                    <li><a href="#">
                        <time datetime="2018-01-01">2018.00.00</time>
                        <span class="cat_02">入間店</span>
                        <em>最新のニュースを掲載します。CMS対応にてお願いいたします。（最大50文字程度）</em>
                      </a></li>
                    <li><a href="#">
                        <time datetime="2018-01-01">2018.00.00</time>
                        <span class="cat_03">札幌北広島店</span>
                        <em>最新のニュースを掲載します。CMS対応にてお願いいたします。（最大50文字程度）</em>
                      </a></li>
                    <li><a href="#">
                        <time datetime="2018-01-01">2018.00.00</time>
                        <span class="cat_04">台湾店</span>
                        <em>最新のニュースを掲載します。CMS対応にてお願いいたします。（最大50文字程度）</em>
                      </a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
    <figure id="airship" class="news_airship show_pc">
      <a href=""><img src="./asset/img/ariship.png" alt="airship"></a>
      <figcaption>採用情報</figcaption>
    </figure>
  </div>
</div>
<div class="what_lil" id="what_lil">
  <figure ><img src="./asset/img/what_lil.svg" alt="what lil"></figure>
</div>
